// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine\Classes\Camera\CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Components\InputComponent.h"
// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

void APlayerPawnBase::AddFood()
{
	float X = rand() % (2 * MaxX) - MaxX;
	float Y = rand() % (2 * MaxY) - MaxY;
	FVector NewLocation(X, Y, MaxZ);
	FTransform NewTransform(NewLocation);
	FoodActor = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	if (FoodActor) {
		FoodActor->PlayerOwner = this;
	}
	
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	AddFood();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN) {
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP) {
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT) {
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT) {
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

